# Maintainer: Laurent Carlier <lordheavym@gmail.com>

pkgname=xf86-video-ati-git
pkgdesc="X.org ati video driver with Glamor acceleration (git version)"
pkgver=3641.8fc442d6
pkgrel=1
groups=('mesagit')
arch=('x86_64')
url="http://xorg.freedesktop.org/"
license=('custom')
depends=('libsystemd' 'mesa-git')
makedepends=('xorg-server-devel' 'git')
conflicts=('xorg-server<21.1.1' 'xf86-video-ati')
groups=('xorg-drivers')
provides=('xf86-video-ati')
source=('git+http://anongit.freedesktop.org/git/xorg/driver/xf86-video-ati.git')
options=('!libtool')
md5sums=(SKIP)

pkgver() {
  cd xf86-video-ati

  echo $(git rev-list --count master).$(git rev-parse --short master)
}

build() {
  cd xf86-video-ati
  
  # Since pacman 5.0.2-2, hardened flags are now enabled in makepkg.conf
  # With them, module fail to load with undefined symbol.
  # See https://bugs.archlinux.org/task/55102 / https://bugs.archlinux.org/task/54845
  export CFLAGS=${CFLAGS/-fno-plt}
  export CXXFLAGS=${CXXFLAGS/-fno-plt}
  export LDFLAGS=${LDFLAGS/,-z,now}

  ./autogen.sh --prefix=/usr --enable-glamor
   make
}

package() {
  cd xf86-video-ati
  
  make "DESTDIR=${pkgdir}" install
  install -Dm644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/license"
}
